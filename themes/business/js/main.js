function scrollToInit(){
  $('a[href*="#"]:not(".mod"), button[data-href^="#"]').click(function(){
    var href = $(this).attr('href') ? $(this).attr('href').slice(1) : $(this).attr('data-href');
    $.scrollTo( href, 1000);
    $('.b-navbar .navbar-collapse').removeClass('in').addClass('collapse');
    return false;
  });
}
