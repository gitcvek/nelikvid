<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode ($this->getPageTitle ()); ?></title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="../../flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="../../flat-ui/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="../../flat-ui/css/flat-ui.css">
        <link rel="stylesheet" href="../../common-files/css/icon-font.css">
        <link rel="stylesheet" href="../../common-files/css/animations.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-blog/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-contacts/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-content/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-crew/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-footer/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-header/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-price/css/style.css">
        <link rel="stylesheet" href="../../ui-kit/ui-kit-projects/css/style.css">
        <link rel="stylesheet" href="themes/business/css/font-awesome.min.css">
        <?php
                if(YII_DEBUG) {
                        Yii::app ()->assetManager->publish (YII_PATH . '/web/js/source',false,-1,true);
                }
                Yii::app ()->clientScript->registerCoreScript ('jquery');
        ?>
</head>
<body>
<div class="page-wrapper">
<header class="header-4 v-center bg-midnight-blue">
        <div class="background">&nbsp;</div>
        <div>
                <div class="container">
                        <div class="row">
                                <div class="col-sm-6">
                                        <div class="hero-unit">
                                                <h1>Продать неликвидные запчасти</h1>

                                                <p>Скопились нереализованные запчасти? мы поможем вам их продать!</p>
                                        </div>
                                        <div class="btns">
                                                <a class="btn btn-large btn-primary" href="/#contact">Продать неликвид</a>
                                                <a class="btn btn-large btn-gray" href="/#contact">Задать вопрос</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</header>
<!-- content-10 -->
<section class="content-10" id="pluses">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6">
                                <h3>Мы избавим Вас от неликвидных остатков!</h3>

                                <p>Вы можете добавить нереализованные товары своего магазина в общую базу данных и мы найдем вам покупателя.</p>

                                <div class="features">
                                        <div class="box">
                                                <img src="../../common-files/icons/retina@2x.png" width="100" height="100" alt="">
                                                <h6>Больше не будет убытков</h6>

                                                <p>У любого оптовика или поставщика имеется складской запас, но есть и неликвид.
                                                        Остатки и неликвид остаются невостребованными, их очень трудно реализовать, а продавец несет убытки.
                                                </p>
                                        </div>
                                        <div class="box">
                                                <img src="../../common-files/icons/Gift-Box@2x.png" width="100" height="100" alt="">
                                                <h6>Быстрый поиск редких запчастей</h6>

                                                <p>К Вам пришел клиент, а у вас нет нужной запчасти в наличии?
                                                        Воспользуйтесь единой базой остатков, чтобы быстро найти ее у партнеров. </p>
                                        </div>
                                        <div class="box">
                                                <img src="../../common-files/icons/time@2x.png" width="100" height="100" alt="">
                                                <h6>Рост продаж</h6>

                                                <p>Есть неликвид, а кому-то он понадобился? Продайте его через единую систему.</p>
                                        </div>
                                </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-2 text-center">
                                <div id="c-10_myCarousel" class="carousel slide">
                                        <div class="carousel-inner">
                                                <div class="item active">
                                                        <img src="../../common-files/img/content/iphone@2x.png" width="302" height="635" alt="">
                                                </div>
                                                <div class="item">
                                                        <img src="../../common-files/img/content/iphone@2x.png" width="302" height="635" alt="">
                                                </div>
                                                <div class="item">
                                                        <img src="../../common-files/img/content/iphone@2x.png" width="302" height="635" alt="">
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<!-- content-23 -->
<section class="content-23 bg-midnight-blue">
        <div class="holder v-center">
                <div>
                        <div class="container">
                                <div class="hero-unit hero-unit-bordered">
                                        <h1>Продать неликвид!</h1>
                                </div>
                        </div>
                </div>
        </div>
        <a class="control-btn fui-arrow-down" href="#features"></a>
</section>
<!-- content-30 -->
<section class="content-30" id="features">
        <div class="container">
                <div class="row features">
                        <div class="col-sm-3 box-1">
                                <div class="description-top">
                                        <h6>Легко!</h6>
                                        Для того, чтобы сбыть неликвид вам всего лишь нужно заполнить простую форму на нажем сайте.
                                        <img src="../../common-files/img/content/arrow-tl@2x.png" width="61" height="97" alt="">
                                </div>
                                <div class="img">
                                        <img src="../../common-files/img/content/screen-1-bw@2x.png" width="220" height="391" alt="">
                                        <img src="../../common-files/img/content/screen-1@2x.png" width="220" height="391" alt="">
                                </div>
                                <div class="description-bottom">
                                        <h6>Быстро!</h6>
                                        В кратчайшие сроки мы найдем вам покупателя.
                                        <img src="../../common-files/img/content/arrow-bl@2x.png" width="97" height="110" alt="">
                                </div>
                        </div>
                        <div class="col-sm-3 box-2">
                                <div class="description-top">
                                        <h6>Покупатели</h6>
                                        На основе данных закупа и продаж мы достаточно точно знаем кому предложить вашу продукцию, то есть самых теплых из покупателей.
                                        <img src="../../common-files/img/content/arrow-tl@2x.png" width="61" height="97" alt="">
                                </div>
                                <div class="img">
                                        <img src="../../common-files/img/content/screen-2-bw@2x.png" width="220" height="391" alt="">
                                        <img src="../../common-files/img/content/screen-2@2xc-30.png" width="220" height="391" alt="">
                                </div>
                                <div class="description-bottom">
                                        <h6>Спрос в вашем регионе</h6>
                                        Мы предоставляем аналитические данные, по статистике спроса запчастей в регионах, по брендам, позициям запчастей, ценам.
                                        <img src="../../common-files/img/content/arrow-br@2x.png" width="97" height="110" alt="">
                                </div>
                        </div>
                        <div class="col-sm-3 box-3">
                                <div class="description-top">
                                        <h6>Поток клиентов</h6>
                                        Мы направляем поток клиентов из автосервисов и помогаем сбыть неликвидный товар.
                                        <img src="../../common-files/img/content/arrow-tr@2x.png" width="61" height="97" alt="">
                                </div>
                                <div class="img">
                                        <img src="../../common-files/img/content/screen-3-bw@2x.png" width="220" height="391" alt="">
                                        <img src="../../common-files/img/content/screen-3@2x.png" width="220" height="391" alt="">
                                </div>
                                <div class="description-bottom">
                                        <h6>Скорость обслуживания</h6>
                                        Также помогаем быстро и качественно обслужить клиента, даже если у вас нет нужных запчастей. Это приводит к повышению лояльности.
                                        <img src="../../common-files/img/content/arrow-bl@2x.png" width="97" height="110" alt="">
                                </div>
                        </div>
                        <div class="col-sm-3 box-4">
                                <div class="description-top">
                                        <h6>Продавцы</h6>
                                        Предоставляем только проверенных продавцов, все продавцы проходят службу безопасности.
                                        <img src="../../common-files/img/content/arrow-tr@2x.png" width="61" height="97" alt="">
                                </div>
                                <div class="img">
                                        <img src="../../common-files/img/content/screen-4-bw@2x.png" width="220" height="391" alt="">
                                        <img src="../../common-files/img/content/screen-4@2x.png" width="220" height="391" alt="">
                                </div>
                                <div class="description-bottom">
                                        <h6>Поиск аналогов</h6>
                                        Мы помогаем быстро подобрать аналоги по брендам и производителям.
                                        <img src="../../common-files/img/content/arrow-br@2x.png" width="97" height="110" alt="">
                                </div>
                        </div>
                </div>
        </div>
</section>
<!-- contacts-2 -->
<?php $this->widget ('BlockWidget',array("place" => SiteModule::PLACE_BOTTOM)); ?>
<!-- footer-2 -->
<footer class="footer-2 bg-midnight-blue">
        <div class="container">
                <nav class="pull-left">
                        <ul>
                        </ul>
                </nav>
                <div class="social-btns pull-right">
                        <a href="#">
                                <div class="fui-vimeo"></div>
                                <div class="fui-vimeo"></div>
                        </a>
                        <a href="#">
                                <div class="fui-facebook"></div>
                                <div class="fui-facebook"></div>
                        </a>
                        <a href="#">
                                <div class="fui-twitter"></div>
                                <div class="fui-twitter"></div>
                        </a>
                </div>
                <div class="additional-links">
                </div>
        </div>
</footer>
</div>
<?php $this->widget ('BlockWidget',array("place" => SiteModule::PLACE_FOOTER)); ?>
<!-- Placed at the end of the document so the pages load faster -->
<script src="../../flat-ui/js/bootstrap.min.js"></script>
<script src="../../common-files/js/modernizr.custom.js"></script>
<script src="../../common-files/js/page-transitions.js"></script>
<script src="../../common-files/js/startup-kit.js"></script>
<script src="/themes/business/js/jquery.scrollTo-min.js"></script>
<script src="/themes/business/js/main.js"></script>
        <script type="text/javascript">
            jQuery(function(){
                scrollToInit();
            });
        </script>

</body>
</html>